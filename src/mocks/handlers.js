import { rest } from 'msw'

let metricsObjMock = {"metrics": [{"timestamp": "2021-11-12T19:35:32.513421-08:00", "name": "Wasabi", "id": 7}, {"timestamp": "2021-11-12T19:35:20.810082-08:00", "name": "Sushi", "id": 6}, {"timestamp": "2021-09-02T12:56:25.706236-07:00", "name": "bunz", "id": 4}]};
let recordSetMock = {"values": [{"value": 112.0, "timestamp": "2021-11-14T17:53:27.304159"}, {"value": 3323.0, "timestamp": "2021-11-14T17:52:16.029710"}, {"value": 1223.0, "timestamp": "2021-11-14T17:48:05.256306"}, {"value": 2323222.0, "timestamp": "2021-11-14T17:47:56.322461"}, {"value": 3434234.0, "timestamp": "2021-11-13T00:29:51.988417"}, {"value": 23234.0, "timestamp": "2021-11-12T23:28:33.043661"}, {"value": 12123123.0, "timestamp": "2021-11-12T22:18:55.602537"}, {"value": 2334234.0, "timestamp": "2021-11-12T21:57:03.662308"}]}
export const handlers = [
    rest.get('http://127.0.0.1:5000/metrics', (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json(
                metricsObjMock
            )
        )
    }),
    rest.get(`http://127.0.0.1:5000/metrics/7/recordset`, (req, res, ctx) => {
        return res(
            ctx.status(200),
            ctx.json(
                recordSetMock
            )
        )
    }),
    rest.post('http://127.0.0.1:5000/metrics', (req, res, ctx) => {
        const { name } = req.body;
        return res(
            ctx.status(201),
            ctx.json(
                {"name": name, "timestamp": "2021-11-12T19:35:32.513421-08:00", "id": 7}
            )
        )
    }),
]