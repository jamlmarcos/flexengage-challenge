import React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useState } from "react";

export default function SharedDialog({ isOpen, handleClose, title, submit, textFieldConfig }) {
    const [val, setVal] = useState('');

    const handleSubmit = () => {
        submit(val);
        handleClose();
    }

    const handleTextFieldChange = (e) => {
        setVal(e.target.value);
    }

    return (
        <div>
            <Dialog open={isOpen} onClose={handleClose}>
                <DialogTitle>{ title }</DialogTitle>
                <DialogContent>
                    <TextField
                        onChange={ handleTextFieldChange }
                        autoFocus
                        margin="dense"
                        placeholder='Enter Value'
                        id={ textFieldConfig.id }
                        label={ textFieldConfig.label }
                        type={ textFieldConfig.type }
                        fullWidth
                        variant="standard"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={ handleClose }>Cancel</Button>
                    <Button onClick={ handleSubmit }>Create</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}