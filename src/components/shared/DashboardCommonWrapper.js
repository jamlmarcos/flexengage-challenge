import SharedDialog from "./SharedDialog";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import React, {useState} from "react";
import PropTypes from "prop-types";

export default function DashboardCommonWrapper ({ children, ...props}) {
    const [createModalOpen, setCreateModalOpen] = useState(false);

    const handleClose = () => {
        setCreateModalOpen(false);
    };

    return (
        <main>
            <SharedDialog
                isOpen={ createModalOpen }
                handleClose={ handleClose }
                title={ props.config.dialogFieldConfig.label }
                submit={ props.config.handleCreate }
                textFieldConfig={ props.config.dialogFieldConfig }/>
            <Box
                sx={{
                    bgcolor: 'background.paper',
                    pt: 8,
                    pb: 6,
                }}
            >
                <Container>
                    <Typography
                        component="h2"
                        variant="h3"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    >
                        {props.config.pageLabel}
                    </Typography>
                    <Stack
                        sx={{ pt: 4 }}
                        direction="row"
                        spacing={2}
                        justifyContent="center"
                    >
                        <Button onClick={() => { setCreateModalOpen(true) }} variant="contained">{ props.config.dialogFieldConfig.label }</Button>
                    </Stack>
                </Container>
                {children}
            </Box>
        </main>

    )
};
DashboardCommonWrapper.propTypes = {
    config: PropTypes.shape({
        dialogFieldConfig: PropTypes.shape({
            id: PropTypes.string,
            label: PropTypes.string,
            type: PropTypes.string,
        }),
        handleCreate: PropTypes.func,
        pageLabel: PropTypes.string,
    })
};