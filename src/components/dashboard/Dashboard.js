import React from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { Divider } from "@mui/material";
import { useNavigate } from "react-router-dom";

import { createMetric, deleteMetric } from "../../redux/thunks";

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import DashboardCommonWrapper from "../shared/DashboardCommonWrapper";
import { store, useAppSelector } from "../../redux/store";

export default function Dashboard() {
    const dispatch = store.dispatch;
    const { metrics } = useAppSelector(state => state.metrics);

    const navigate = useNavigate();

    const handleDeleteMetric = (id) => {
        dispatch(deleteMetric({id: id}));
    }

    const handleCreate = (name) => {
        dispatch(createMetric({
            name
        }));
    }

    const dialogFieldConfig = {
        id: 'create-metric-dialog',
        label: 'Create Metric',
        type: 'text'
    }

    const wrapperConfig = {
        dialogFieldConfig,
        handleCreate,
        pageLabel: 'Your Metrics Dashboard'
    }

    return (
        <DashboardCommonWrapper config={ wrapperConfig } >
            <Container sx={{ py: 8 }} maxWidth="md">
                {/* End hero unit */}
                <Grid container spacing={4}>
                    {metrics.map((metric) => (
                        <Grid item key={metric.id} xs={12} sm={6} md={4}>
                            <Card
                                sx={{ height: '100%', display: 'flex', flexDirection: 'column' }}
                            >
                                <CardContent sx={{ flexGrow: 1 }}>
                                    <Typography gutterBottom variant="h5" component="h2">
                                        {metric.name}
                                    </Typography>
                                    <Divider />
                                </CardContent>
                                <CardActions>
                                    <Button
                                        onClick={() => handleDeleteMetric(metric.id)}
                                        size="small"
                                        variant="outlined"
                                        color="error"
                                    >Delete
                                    </Button>
                                    <Button
                                        onClick={() => navigate(`/metric/${metric.id}`)}
                                    >Open
                                    </Button>
                                </CardActions>
                            </Card>
                        </Grid>
                    ))}
                </Grid>
            </Container>
        </DashboardCommonWrapper>
    );
}