import React from 'react';
import Container from "@mui/material/Container";
import { store, useAppSelector } from "../../redux/store";
import { useParams } from "react-router";
import { useEffect } from "react";

import { createRecordSet, retrieveRecordSets } from "../../redux/thunks";

import RecordSetTable from "./RecordSetTable";
import DashboardCommonWrapper from "../shared/DashboardCommonWrapper";

export default function MetricDetail() {
    const dispatch = store.dispatch;
    const { recordSets } = useAppSelector(state => state.metrics);
    let { id } = useParams();

    useEffect(() => {
        dispatch(retrieveRecordSets({ id }));
    }, [dispatch, id])

    const handleCreate = (value) => {
        dispatch(createRecordSet({
            id,
            data: { value }
        }));
    }

    const dialogFieldConfig = {
        id: 'create-record-set-dialog',
        label: `Create Record Set for Metric: ${ id }`,
        type: 'number'
    }

    const wrapperConfig = {
        dialogFieldConfig,
        handleCreate,
        pageLabel: 'Metrics Details'
    }

    return (
        <DashboardCommonWrapper config={ wrapperConfig } >
            <Container maxWidth="sm">
                <RecordSetTable recordSets={ recordSets }/>
            </Container>
        </DashboardCommonWrapper>
    )
}