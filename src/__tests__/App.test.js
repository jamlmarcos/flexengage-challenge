import { cleanup, fireEvent, render, screen } from '@testing-library/react';

import App from "../App";
import { createTestStore } from "../redux/store";
import { Provider } from "react-redux";
import { retrieveMetrics, retrieveRecordSets } from "../redux/thunks";

let store;
describe("Dashboard", () => {
    beforeEach(() => {
        store = createTestStore();
    });

    afterEach(() => {
        cleanup();
    });

    it('should render the Metrics Dashboard', async () => {
        render(
            <Provider store={store}>
                <App />
            </Provider>
        );

        const metricsDash = screen.getByText('Your Metrics Dashboard');
        expect(metricsDash).toBeVisible();
    });

    it('should display the Create Metric modal/dialog when clicking the "CREATE METRIC" button', async () => {
        store.dispatch(retrieveMetrics())
        render(
            <Provider store={store}>
                <App />
            </Provider>
        );
        const metricsDash = screen.getByText('Your Metrics Dashboard');
        expect(metricsDash).toBeVisible();

        const cards = await screen.findAllByText(/OPEN/i);

        const createMetricButton = screen.getByText(/CREATE METRIC/i);
        fireEvent.click(createMetricButton);

        const createMetricInput = screen.getByPlaceholderText(/Enter Value/i);

        fireEvent.change(createMetricInput, { target: { value: { name: 'Created With MSW'} }});
        const modalCreateButton = screen.getByRole('button', { name: "Create" });

        expect(modalCreateButton).toBeVisible();

    });

    describe('Metrics Details Page', () => {

        it('should route to Metrics Details page and display 8 metric details items as per test API call', async function () {
            store.dispatch(retrieveMetrics());
            render(
                <Provider store={store}>
                    <App/>
                </Provider>
            );

            const cards = await screen.findAllByText(/OPEN/i);
            fireEvent.click(cards[0]);
            store.dispatch(retrieveRecordSets({ id: 7}))

            const detailsLabel = await screen.findByText(/Metrics Details/i);

            const detailItems = await screen.findAllByRole('cell');
            expect(detailItems.length).toBe(8);
        });

        it('should display the timestamp in the correct formage, e.g., 11/12/2021 21:57:03', async function () {
            store.dispatch(retrieveMetrics());
            render(
                <Provider store={store}>
                    <App/>
                </Provider>
            );

            store.dispatch(retrieveRecordSets({ id: 7}))

            await screen.findByText('11/12/2021 21:57:03');
        });
    });

});