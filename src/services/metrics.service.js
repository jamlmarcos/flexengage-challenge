import http from "../http-common";

class MetricsService {
    getAllMetrics() {
        return http.get("/metrics");
    }

    getMetric(id) {
        return http.get(`/metrics/${id}`);
    }

    createMetric(data) {
        return http.post("/metrics", data);
    }

    updateMetric(id, name) {
        return http.put(`/metrics/${id}`, { name });
    }

    deleteMetric(id) {
        return http.delete(`/metrics/${id}`);
    }

    createRecordSet(id, data) {
        return http.post(`/metrics/${id}/recordset`, data);
    }

    getRecordSet(id) {
        return http.get(`/metrics/${id}/recordset`);
    }
}

export default new MetricsService();