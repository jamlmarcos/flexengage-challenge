import Dashboard from "./components/dashboard/Dashboard";
import { BrowserRouter, NavLink, Route, Routes } from "react-router-dom";
import MetricDetail from "./components/metricDetail/MetricDetail";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import DehazeIcon from '@mui/icons-material/Dehaze';
import Typography from "@mui/material/Typography";
import * as React from "react";
import {createTheme} from "@mui/material/styles";
import {ThemeProvider} from "@emotion/react";
import {useEffect} from "react";
import {retrieveMetrics} from "./redux/thunks";
import { store } from "./redux/store";
import metrics from "./redux/metrics";

function App() {
  const dispatch = store.dispatch;

  useEffect(() => {
    dispatch(retrieveMetrics());
  }, [dispatch])

  const theme = createTheme();
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <ThemeProvider theme={theme}>
            <CssBaseline />
            <AppBar position="relative">
              <Toolbar>
                <DehazeIcon sx={{ mr: 2 }} />
                <Typography variant="h6" color="inherit" noWrap>
                  <NavLink to='/' style={{textDecoration: 'none', color: 'white'}}>Metrics Dashboard</NavLink>
                </Typography>
              </Toolbar>
            </AppBar>
          </ThemeProvider>
          <Routes>
            <Route path="/" element={<Dashboard metrics={ metrics }/>} />
            <Route path="metric/:id" element={<MetricDetail />} />
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  );
}

export default App;
