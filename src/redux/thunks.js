import {createAsyncThunk} from "@reduxjs/toolkit";
import MetricsService from "../services/metrics.service";

export const createMetric = createAsyncThunk(
    "metrics/create",
    async ({ name }) => {
        const res = await MetricsService.createMetric({ name });
        return res.data;
    }
);

export const retrieveMetrics = createAsyncThunk(
    "metrics/retrieve",
    async () => {
        const res = await MetricsService.getAllMetrics();
        return res.data;
    }
);

export const updateMetric = createAsyncThunk(
    "metrics/update",
    async ({ id, name }) => {
        const res = await MetricsService.updateMetric(id, name);
        return res.data;
    }
);

export const deleteMetric = createAsyncThunk(
    "metrics/delete",
    async ({ id }) => {
        await MetricsService.deleteMetric(id);
        return { id };
    }
);

export const createRecordSet = createAsyncThunk(
    "recordSets/create",
    async ({ id, data }) => {
        const res = await MetricsService.createRecordSet(id, data);
        return res.data;
    }
);

export const retrieveRecordSets = createAsyncThunk(
    "recordSets/retrieve",
    async ({ id }) => {
        const res = await MetricsService.getRecordSet(id);
        return res.data;
    }
);