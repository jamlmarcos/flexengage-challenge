import { createSlice } from "@reduxjs/toolkit";
import {createMetric, createRecordSet, deleteMetric, retrieveMetrics, retrieveRecordSets, updateMetric} from "./thunks";

const initialState = {
    loading: false,
    metrics: [],
    recordSets: [],
};

const metricsSlice = createSlice({
    name: "metrics",
    initialState,
    extraReducers: {
        [retrieveMetrics.pending]: state => {
            state.loading = true;
        },
        [retrieveMetrics.rejected]: (state, action) => {
            state.error = action.payload;
        },
        [retrieveMetrics.fulfilled]: (state, action) => {
            state.loading = false;
            state.metrics = action.payload.metrics;
        },
        [createMetric.pending]: (state) => {
            state.loading = true;
        },
        [createMetric.rejected]: (state, action) => {
            state.error = action.payload;
        },
        [createMetric.fulfilled]: (state, action) => {
            state.loading = false;
            state.metrics = [ action.payload, ...state.metrics]
        },
        [updateMetric.fulfilled]: (state, action) => {
            const index = state.metrics.findIndex(metric => metric.id === action.payload.id);
            state.metrics[index] = {
                ...state.metrics[index],
                ...action.payload,
            };
        },
        [deleteMetric.fulfilled]: (state, action) => {
            let index = state.metrics.findIndex(({ id }) => id === action.payload.id);
            state.metrics.splice(index, 1);
        },
        [createRecordSet.pending]: (state) => {
            state.loading = true;
        },
        [createRecordSet.rejected]: (state, action) => {
            state.error = action.payload.error;
        },
        [createRecordSet.fulfilled]: (state, action) => {
            state.loading = false;
            state.recordSets = [ action.payload, ...state.recordSets]
        },
        [retrieveRecordSets.fulfilled]: (state, action) => {
            state.loading = false;
            state.recordSets = action.payload.values;
        },
    },
});

const { reducer } = metricsSlice;
export default reducer;