import { configureStore } from '@reduxjs/toolkit'
import metricsReducer from './metrics';
import { useSelector } from "react-redux";

export const store = configureStore({
  reducer: { metrics: metricsReducer }
});

export const useAppSelector = useSelector;

export function createTestStore() {
    const store = configureStore({
        reducer: { metrics: metricsReducer },
    })
    return store;
}